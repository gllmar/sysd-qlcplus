# sysd-qlcplus

## Service Name : qlcplus@

where @ is the path relative to home


## Install via curl

```
mkdir -p ~/.config/systemd/user && curl -o ~/.config/systemd/user/qlcplus@.service https://gitlab.com/gllmar/sysd-qlcplus/-/raw/main/qlcplus@.service?ref_type=heads && systemctl --user daemon-reload
```

## Configure instance with path, enable then start

```
[[ $1 ]] && qxw_file_path=$1 || read -p "Enter the path relative to home folder to the QLC+ project file: " qxw_file_path; systemctl --user restart qlcplus@$(systemd-escape "$qxw_file_path") && systemctl --user enable qlcplus@$(systemd-escape "$qxw_file_path")
```

## Stop all instances

```
for service in $(systemctl --user list-units --type=service | grep 'qlcplus@' | awk '{print $1}'); do systemctl --user stop $service; done
```


## Disable all instances

```
for service in $(systemctl --user list-units --type=service | grep 'qlcplus@' | awk '{print $1}'); do systemctl --user disable $service; done
```


## Uninstall

``` 
for service in $(systemctl --user list-units --type=service | grep 'qlcplus@' | awk '{print $1}'); do systemctl --user stop $service; done && for service in $(systemctl --user list-units --type=service | grep 'qlcplus@' | awk '{print $1}'); do systemctl --user disable $service; done && rm ~/.config/systemd/user/qlcplus@.service
```


